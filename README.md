# Lekcja 1

## Wstęp

Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji
umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:

1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,
2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,
3. **XML** (_Extensible Markup Language_) - uniwersalnym języku znaczników przeznaczonym
do reprezentowania różnych danych w ustrukturalizowany sposób.
Przykład kodu html i jego interpretacja w przeglądarce:

<!DOCTYPE html>
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8" />
    <title>Przykład</title>
    </head>
    <body>
    <p> Jakiś paragraf tekstu</p>
    </body>
    </html>
Przykład kodu _Latex_ i wygenerowanego pliku w formacie _pdf_

\\<span style="color:red">documentclass</span>[]{<span style="color:blue">letter</span>}<br>
\\<span style="color:red">usepackage</span>{<span style="color:blue">   lipsum</span>}<br>
\\<span style="color:red">usepackage</span>{<span style="color:blue">polyglossia</span>}<br>
\\<span style="color:red">setmainlanguage</span>{<span style="color:blue">polish</span>}<br>
\\<span style="color:blue">begin</span>{<span style="color:blue">document</span>}<br>
\\<span style="color:blue">begin</span>{<span style="color:blue">letter</span>}{<span style="color:blue">Szanowny Panie XY</span>}<br>
\\<span style="color:red">address</span>{<span style="color:blue">Adres do korespondencji</span>}<br>
\\<span style="color:red">opening</span>{}<br>
\\<span style="color:red">lipsum</span>[2]<br>
\\<span style="color:red">signature</span>{<span style="color:blue">Nadawca</span>}<br>
\\<span style="color:red">closing</span>{<span style="color:blue">Pozdrawiam</span>}<br>
\\<span style="color:blue">end</span>{<span style="color:blue">letter</span>}<br>
\\<span style="color:blue">end</span>{<span style="color:blue">document</span>}<br>

Przykład kodu XML – fragment dokumentu SVG (Scalar Vector Graphics)

    <!DOCTYPE html>
    <html>
    <body>
    <svg height="100" width="100">
     <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
    </svg>
     </body>
    </html>

<!DOCTYPE html>
<html>
<body>
<svg height="100" width="100">
 <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
</svg>
 </body>
</html>

W tym przypadku mamy np. znacznik np. \<_circle_> opisujący parametry koła i który może być
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).

Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z
rozszerzeniem _docx_, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

$<span style="color:green">unzip</span> -l <span style="color:purple"> "test.docx</span> <br>
Archive: <span style="color:purple">"test.docx</span>
| Length| <span style="color:pink">Date <span> |Time |Name|
|---------| ----------| -----| ----|
 573 |2020-10-11| 18:20| _rels/.rels
 731 |2020-10-11| 18:20| docProps/core.xml
 508 |2020-10-11| 18:20| docProps/app.xml
 531 |2020-10-11| 18:20| word/_rels/document.xml.rels
 1421 |2020-10-11| 18:20| word/document.xml
 2429 |2020-10-11| 18:20| word/styles.xml
 853 |2020-10-11| 18:20| word/fontTable.xml
 241 |2020-10-11| 18:20| word/settings.xml
 1374 |2020-10-11| 18:20| **[** Content_Types **]**.xml

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji
wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By
wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników
służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych
narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych
formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie
używany do tworzenia plików README.md (w projektach open source) i powszechnie
obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John
Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania
i tak w 2016 r. opublikowano dokument [RFC 7764](https://tools.ietf.org/html/rfc7764) który zawiera opis kilku odmian tegoż języka:

* CommonMark,
* GitHub Flavored Markdown (GFM),
* Markdown Extra.

## Podstawy składni

Podany link: [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) zawiera opis
podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki
opis w języku polskim.

### **Definiowanie nagłówków**

W tym celu używamy znaków kratki: #

Lewe okno zawiera kod źródłowy – prawe -podgląd

przetworzonego tekstu

### **Definiowanie list**

Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.

Listy nienumerowane definiujemy znakami: *,+,-

### ****Wyróżnianie tekstu****

### ****Tabele****

Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:
Odnośniki do zasobów

[odnośnik do zasobów\](www.gazeta.pl)<br>
[odnośnik do pliku\](LICENSE.md)<br>
[odnośnik do kolejnego zasobu][1]<br>
[1\]: http://google,com

### ****Obrazki****

!\[alt text](https://server.com/images/icon48.png "Logo 1") – obrazek z zasobów
internetowych<br>

!\[](logo.png) – obraz z lokalnych zasobów<br>

### ****Kod źródłowy dla różnych języków programowania****

### ****Tworzenie spisu treści na podstawie nagłówków****

### ****Edytory dedykowane****

Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w
dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi:

1. Edytor Typora - [https://typora.io/](https://typora.io/)
2. Visual Studio Code z wtyczką „markdown preview”

### **Pandoc – system do konwersji dokumentów Markdown do innych formatów**

Jest oprogramowanie typu _open source_ służące do konwertowania dokumentów pomiędzy
różnymi formatami.

Pod poniższym linkiem można obejrzeć przykłady użycia:<br>

[https://pandoc.org/demos.html](https://pandoc.org/demos.html)

Oprogramowanie to można pobrać z spod adresu: [https://pandoc.org/installing.html](https://pandoc.org/installing.html)

Jeżeli chcemy konwertować do formatu _latex_ i _pdf_ trzeba doinstalować oprogramowanie
składu _Latex_ \(np. Na MS Windows najlepiej sprawdzi się Miktex - https\:/\/miktex\.org\/\)

Gdyby podczas konwersji do formatu _pdf_ pojawił się komunikat o niemożliwości<br>
znalezienia programu pdflatex rozwiązaniem jest wskazanie w zmiennej środowiskowej
**PATH** miejsca jego położenia

Pod adresem (_<https://gitlab.com/mniewins66/templatemn.git>_) znajduje się przykładowy plik
Markdown z którego można wygenerować prezentację w formacie _pdf_ wykorzystując
klasę latexa _beamer_.

W tym celu należy wydać polecenie z poziomu terminala:

$pandoc templateMN.md -t beamer -o prezentacja.pdf
